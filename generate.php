<?php

ini_set('max_execution_time', 900);

require_once 'include/helper/Num2Str.php';
require_once 'vendor/autoload.php';
require_once 'functions.php';

use Zend\Text;

$language = array();
$language['table']['header']['col_num'] = 'Номер';
$language['table']['header']['col_material_title'] = 'Название товара / материала / группы требований';
$language['table']['header']['col_parameter_title'] = 'Название параметра / показателя / характеристики';
$language['table']['header']['col_parameter_num'] = 'Номер параметра / показателя / характеристики';
$language['table']['header']['col_min_value'] = 'Требования к минимальному значению показателя';
$language['table']['header']['col_max_value'] = 'Требования к максимальному значению показателя';
$language['table']['header']['col_fixed_value'] = 'Требования к значениям показателя, которые не могут изменяться*';
$language['table']['header']['col_requirements'] = 'Требования к значениям показателя(ей)';
$language['table']['header']['col_proposal_values'] = 'Предложение участника закупки**';
$language['table']['header']['col_proposal_trademark'] = 'Предложение участника закупки [указание на товарный знак (его словесное обозначение) (при наличии), знак обслуживания (при наличии), фирменное наименование (при наличии), патенты (при наличии), полезные модели (при наличии), промышленные образцы (при наличии) товара]**';
$language['table']['header']['col_proposal_country'] = 'Предложение участника закупки [наименование страны происхождения товара]**';
$language['table']['header']['col_unit'] = 'Единица измерения';

// ==================================================================================
if (!is_object($mysqli)) {
    $projectId = 8791;
    $modeResult = 4;

    $dbConnect = mysqli_connect("192.168.0.140", "user1", "Aq7MDVSNj", "project");
    mysqli_set_charset($dbConnect, "utf8");

    $checkpointNum = getMaxCheckpointNum($dbConnect, $projectId);
} else {
    $dbConnect = $mysqli;
}

$checkpointId = getCheckpointIdByNum($dbConnect, $projectId, $checkpointNum);

switch ($select_instruction_view) {
    default:
    case 'Простая инструкция':
        $bGenerateSimpleInstruction = true;
        $bGenerateParagraphs = false;
        $bGenerateLists = false;
        break;
    case 'Инструкция с параграфами без списков':
        $bGenerateSimpleInstruction = false;
        $bGenerateParagraphs = true;
        $bGenerateLists = false;
        break;
    case 'Инструкция с параграфами со списками':
        $bGenerateSimpleInstruction = false;
        $bGenerateParagraphs = true;
        $bGenerateLists = true;
        break;
    case 'Инструкция без параграфов без списков':
        $bGenerateSimpleInstruction = false;
        $bGenerateParagraphs = false;
        $bGenerateLists = false;
        break;
    case 'Инструкция без параграфов со списками':
        $bGenerateSimpleInstruction = false;
        $bGenerateParagraphs = false;
        $bGenerateLists = true;
        break;
}

switch ($select_instruction_mode) {
    default:
    case 'Материалы и требования названиями':
        $bInstructionMatNames = true;
        $bInstructionReqNames = true;
        break;
    case 'Требования цифрами':
        $bInstructionMatNames = true;
        $bInstructionReqNames = false;
        break;
}

$bConvertReqNumbersToWord = false;
if ($select_view_mode_req_numbers === 'Текст') {
    $bConvertReqNumbersToWord = true;
}

$bConvertAppNumbersToWord = true;
if ($select_view_mode_app_numbers === 'Текст') {
    $bConvertAppNumbersToWord = true;
}

$bFillApp = false;
if ($select_view_mode_app == 'Да') {
    $bFillApp = true;
}

$strResult = "";
$strResult .= "project:$projectId\r\n";
$strResult .= "checkpointId:$checkpointId\r\n";
$strResult .= "checkpointNum:$checkpointNum\r\n";
$strResult .= "\r\n";

$queryMaterials = "
SELECT DISTINCT
	cpm.Order,
    cpm.MaterialId,
    m.Title as MaterialTitle,
    so.NumRandom,
    rc.TradeMark,
    rc.Country
FROM
	project.CommonProjectMaterial as cpm
		left join project.Material as m on m.Id = cpm.MaterialId
        LEFT JOIN _RawCatalog as rc ON  LOWER(CONCAT(', ', rc.MaterialTitle, ',')) REGEXP CONCAT(', ', LOWER(m.Title), '([,]{1}|( -))')

		left join project.SortOrder as so on
		      so.MaterialId = cpm.MaterialId
		      and so.ProjectID = $projectId
		      and so.CheckpointID = $checkpointId
		      and so.SortType = 1
WHERE
	cpm.ProjectId = $projectId
	and cpm.Version = (SELECT max(Version) from project.CommonProjectMaterial where ProjectId = $projectId)
ORDER BY
    so.NumRandom ASC
";

$queryRequirement = "
SELECT DISTINCT
	-- cpm.Order,
    i.MaterialId,
    m.Title as MaterialTitle,
    i.ParameterId,
    p.Title as ParamTitle,
    i.isMinInclude,
    i.MinRange,
    i.isMaxInclude,
    i.MaxRange,
    i.NegativeAssert,
    i.isRange,
    i.FixedValue,
    so.NumRandom as ParamNum, 

    i.OrderValue,
    i.Additional,
    -- rc.TradeMark,
    -- rc.Country,

    u.ShortTitle as UnitShortTitle,
    u.FullTitle as UnitFullTitle,

    i.OrderRange
FROM
	project.Item as i
		left join project.Material as m on m.Id = i.MaterialId
        left join project.Parameter as p on p.Id = i.ParameterId
		-- project.CommonProjectMaterial as cpm
        -- left join _RawCatalog as rc ON m.Title = rc.MaterialTitle

        left join project.Unit as u on u.Id = i.UnitId
		left join project.SortOrder as so on
		      so.MaterialId = i.MaterialId
		      and so.ParameterId = i.ParameterId
		      and so.ProjectID = $projectId
		      and so.CheckpointID = $checkpointId
		      and so.SortType = 2
WHERE
	i.ProjectId = $projectId and
    i.MaterialId = %ID%
    and i.Version = (SELECT max(Version) from project.Item where ProjectId = $projectId)
    and i.IsDeleted <> 1
ORDER BY
    so.NumRandom ASC
    ";

switch ($modeResult) {
    // ----------------------------------------------------- //
    // Текстовый режим
    // ----------------------------------------------------- //
    case 1:
        $queryMaterialsResult = mysqli_query($dbConnect, $queryMaterials);

        while ($rowMaterial = mysqli_fetch_assoc($queryMaterialsResult)) {
            $nextRequirement = "";

            $orderMaterial = $rowMaterial['Order'];
            $idMaterial = $rowMaterial['MaterialId'];
            $titleMaterial = trim($rowMaterial['MaterialTitle']);
            $numMaterialRandom = $rowMaterial['NumRandom'];

            $nextRequirement .= $numMaterialRandom . ". " . $titleMaterial . ".<br>";

            if ($idMaterial > 0) {
                $queryRequirementResult = mysqli_query($dbConnect, str_replace('%ID%', $idMaterial, $queryRequirement));
                while ($rowRequirement = mysqli_fetch_assoc($queryRequirementResult)) {
                    $paramTitle = cleanString($rowRequirement['ParamTitle']);
                    $idParameter = $rowRequirement['ParameterId'];
                    $unitTitle = trim($rowRequirement['UnitShortTitle']);
                    $unitFullTitle = trim($rowRequirement['UnitFullTitle']);

                    $unitTitle <> "" ? $unitString = ' ' . $unitFullTitle : $unitString = '';
                    $rowRequirement['isRange'] ? $rangeModifer = '*' : $rangeModifer = '';

                    if ($bFillApp) {
                        $appValue = getAppValue($rowRequirement, $bConvertAppNumbersToWord);

                        $nextRequirement .= str_replace('  ', ' ', " " . $paramTitle . ": " . $appValue . $unitString . ".\r\n");
                    } else {
                        $nextRequirement .= str_replace('  ', ' ', " " . $paramTitle . " " . getValue($rowRequirement) . $unitString . "." . $rangeModifer);
                    }
                }
                $appTM = getAppTradeMark($rowMaterial);
                $appTM !== '' ?  $nextRequirement .= ' '.$appTM : '';

                $appCountry = getAppCountry($rowMaterial);
                $appCountry !== '' ?  $nextRequirement .= ' '.$appCountry : '';
            }

            if ($bFillApp) {
                $nextRequirement .= "\r\n";
            }

            $strResult .= $nextRequirement;
        }

        break;
    // ----------------------------------------------------- //
    // Таблица с несколькими колонками без rowspan
    // ----------------------------------------------------- //
    case 2:

        $queryMaterialsResult = mysqli_query($dbConnect, $queryMaterials);
        while ($rowMaterial = mysqli_fetch_assoc($queryMaterialsResult)) {
            $table = new Zend\Text\Table\Table([
                'columnWidths' => [15, 15, 15, 15, 15, 15, 15]
            ]);

            $row = new Zend\Text\Table\Row();
            $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_num']));
            $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_material_title']));
            $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_proposal_trademark'], null, 3));
            $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_proposal_country'], null, 2));
            $table->appendRow($row);

            $orderMaterial = $rowMaterial['Order'];
            $idMaterial = $rowMaterial['MaterialId'];
            $titleMaterial = trim($rowMaterial['MaterialTitle']);
            $numMaterialRandom = $rowMaterial['NumRandom'];

            $queryRequirementResult = mysqli_query($dbConnect, str_replace('%ID%', $idMaterial, $queryRequirement));
            $numRequirements = mysqli_num_rows($queryRequirementResult);

            $appTradeMark = "";
            $appCountry = "";
            $appTMC = "";
            if ($bFillApp) {
                $appTradeMark = getAppTradeMark($rowMaterial);
                $appCountry = getAppCountry($rowMaterial);
                $appTradeMark <> "" ? $appTMC = " ($appTradeMark, $appCountry)" : $appTMC = " ($appCountry)";
            }

            $row = new Zend\Text\Table\Row();
            $row->appendColumn(new Zend\Text\Table\Column($numMaterialRandom, null, null, null, $numRequirements + 2));
            $row->appendColumn(new Zend\Text\Table\Column($titleMaterial));
            $row->appendColumn(new Zend\Text\Table\Column($appTradeMark, null, 3));
            $row->appendColumn(new Zend\Text\Table\Column($appCountry, null, 2));
            $table->appendRow($row);

            if ($idMaterial > 0) {
                // Implicitly build rows, by supply an array of column values:
                $table->appendRow(
                    [
                        //'',
                        $language['table']['header']['col_parameter_title'],
                        $language['table']['header']['col_min_value'],
                        $language['table']['header']['col_max_value'],
                        $language['table']['header']['col_fixed_value'],
                        $language['table']['header']['col_proposal_values'],
                        $language['table']['header']['col_unit']
                    ]
                );

                while ($rowRequirement = mysqli_fetch_assoc($queryRequirementResult)) {
                    $paramTitle = cleanString($rowRequirement['ParamTitle']);
                    $idParameter = $rowRequirement['ParameterId'];
                    $unitTitle = trim($rowRequirement['UnitShortTitle']);
                    $unitFullTitle = trim($rowRequirement['UnitFullTitle']);

                    $rangeModifer = '';
                    //$rowRequirement['isRange'] ? $rangeModifer = '*' : $rangeModifer = '';
                    if ($bFillApp) $appValue = getAppValue($rowRequirement, $bConvertAppNumbersToWord); else $appValue = '';

                    $rowRequirement['OrderRange'] ? $rangeIndex = 1 : $rangeIndex = 0;
                    $instruction[$titleMaterial][$rangeIndex][] = $paramTitle;

                    // Or build the row and column manually:
                    $row = new Zend\Text\Table\Row();
                    //$row->appendColumn(new Zend\Text\Table\Column(''));
                    $row->appendColumn(new Zend\Text\Table\Column($paramTitle . $rangeModifer));
                    $row->appendColumn(new Zend\Text\Table\Column(getMinValue($rowRequirement)));
                    $row->appendColumn(new Zend\Text\Table\Column(getMaxValue($rowRequirement)));
                    $row->appendColumn(new Zend\Text\Table\Column(getFixedValue($rowRequirement)));
                    $row->appendColumn(new Zend\Text\Table\Column($appValue));
                    $row->appendColumn(new Zend\Text\Table\Column($unitFullTitle));
                    $table->appendRow($row);
                }
            }

            $strResult .= $table;
        }

        if (!$bFillApp) {
            $strResult .= "* - содержит требования к фиксированному значению показателя (в виде одного или нескольких значений, которым должно соответствовать значения показателя или перечисления допустимых значений показателя, из которых необходимо сделать выбор). В случае требования к фракции является указанием на конкретные фракции в виде интервалов значений.\r\n";
            $strResult .= "** - данные поля являются рекомендуемыми для заполнения участником закупки при использовании предложенной формы и не содержат требований заказчика к показателям/характеристикам товаров/материалов.\r\n";
            $strResult .= "\r\n";

            $strResult .= generateInstruction($instruction, $bGenerateParagraphs, $bGenerateLists) . " ";
        }

        break;
    // ----------------------------------------------------- //
    // Таблица с colspan и общей колонкой требований
    // ----------------------------------------------------- //
    case 3:

        $instruction = array();
        $queryMaterialsResult = mysqli_query($dbConnect, $queryMaterials);
        while ($rowMaterial = mysqli_fetch_assoc($queryMaterialsResult)) {
            $orderMaterial = $rowMaterial['Order'];
            $idMaterial = $rowMaterial['MaterialId'];
            $titleMaterial = trim($rowMaterial['MaterialTitle']);
            $numMaterialRandom = $rowMaterial['NumRandom'];

            if ($idMaterial > 0) {
                $table = new Zend\Text\Table\Table([
                    'columnWidths' => [7, 20, 40, 30, 12]
                ]);

                $queryRequirementResult = mysqli_query($dbConnect, str_replace('%ID%', $idMaterial, $queryRequirement));
                $numRequirements = mysqli_num_rows($queryRequirementResult);

                $row = new Zend\Text\Table\Row();
                $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_num']));
                $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_material_title']));
                $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_proposal_trademark']));
                $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_proposal_country'], null, 2));
                $table->appendRow($row);

                $appTradeMark = '';
                $appCountry = '';
                $appTMC = '';
                if ($bFillApp) {
                    $appTradeMark = getAppTradeMark($rowMaterial);
                    $appCountry = getAppCountry($rowMaterial);

                    $appTradeMark <> "" ? $appTMC = " ($appTradeMark, $appCountry)" : $appTMC = " ($appCountry)";
                }

                $row = new Zend\Text\Table\Row();
                $row->appendColumn(new Zend\Text\Table\Column($numMaterialRandom, null, null, null, $numRequirements + 2));
                $row->appendColumn(new Zend\Text\Table\Column($titleMaterial));
                $row->appendColumn(new Zend\Text\Table\Column($appTradeMark));
                $row->appendColumn(new Zend\Text\Table\Column($appCountry, null, 2));
                $table->appendRow($row);

                // Implicitly build rows, by supply an array of column values:
                $table->appendRow(
                    [
                        //'',
                        $language['table']['header']['col_parameter_title'],
                        $language['table']['header']['col_requirements'],
                        $language['table']['header']['col_proposal_values'],
                        $language['table']['header']['col_unit']
                    ]
                );

                while ($rowRequirement = mysqli_fetch_assoc($queryRequirementResult)) {
                    $paramTitle = cleanString($rowRequirement['ParamTitle']);
                    $idParameter = $rowRequirement['ParameterId'];
                    $unitTitle = trim($rowRequirement['UnitShortTitle']);
                    $unitFullTitle = trim($rowRequirement['UnitFullTitle']);

                    $rangeModifer = '';
                    if ($bGenerateSimpleInstruction && $rowRequirement['OrderRange']){
                        $rangeModifer = '***';
                    }
                    $rowRequirement['OrderRange'] ? $rangeIndex = 1 : $rangeIndex = 0;

                    $instruction[$titleMaterial][$rangeIndex][] = $paramTitle;
                    $appValue = '';
                    if ($bFillApp) {
                        $appValue = getAppValue($rowRequirement, $bConvertAppNumbersToWord);
                    }

                    // Or build the row and column manually:
                    $row = new Zend\Text\Table\Row();
                    //$row->appendColumn(new Zend\Text\Table\Column(''));
                    $row->appendColumn(new Zend\Text\Table\Column($paramTitle . $rangeModifer));
                    $row->appendColumn(new Zend\Text\Table\Column(getValue($rowRequirement, true, $bConvertReqNumbersToWord)));
                    $row->appendColumn(new Zend\Text\Table\Column($appValue));
                    $row->appendColumn(new Zend\Text\Table\Column($unitFullTitle));
                    $table->appendRow($row);
                }

                $strResult .= $table;
            } // end if idMaterial <> ''

        }

        if (!$bFillApp) {
            //$strResult .=  "* - содержит требования к фиксированному значению показателя (в виде одного или нескольких значений, которым должно соответствовать значения показателя или перечисления допустимых значений показателя, из которых необходимо сделать выбор). В случае требования к фракции является указанием на конкретные фракции в виде интервалов значений.\r\n";
            $strResult .= "** - данные поля являются рекомендуемыми для заполнения участником закупки при использовании предложенной формы и не содержат требований заказчика к показателям/характеристикам товаров/материалов.\r\n";
            $strResult .= "\r\n";

            $strResult .= generateInstruction($instruction, $bGenerateParagraphs, $bGenerateLists) . " ";
        }

        break;
    // ----------------------------------------------------- //
    // Таблица c rowspan и 3 колонками требований
    // ----------------------------------------------------- //
    case 4:

        $table = new Zend\Text\Table\Table([
            'columnWidths' => [5, 20, 15, 15, 15, 15, 15, 15]
        ]);

        $instruction = array();

        $row = new Zend\Text\Table\Row();
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_num']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_material_title']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_parameter_title']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_min_value']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_max_value']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_fixed_value']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_proposal_values']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_unit']));
        $table->appendRow($row);

        $queryMaterialsResult = mysqli_query($dbConnect, $queryMaterials);
        $numMaterial = 0;
        while ($rowMaterial = mysqli_fetch_assoc($queryMaterialsResult)) {

            $orderMaterial = $rowMaterial['Order'];
            $idMaterial = $rowMaterial['MaterialId'];
            $titleMaterial = trim($rowMaterial['MaterialTitle']);
            $numMaterialRandom = $rowMaterial['NumRandom'];

            // echo "next material:  $orderMaterial. $titleMaterial\r\n";

            if ($idMaterial > 0) {
                $queryRequirementResult = mysqli_query($dbConnect, str_replace('%ID%', $idMaterial, $queryRequirement));
                $numRequirements = mysqli_num_rows($queryRequirementResult);
                $numRequirement = 0;

                //echo  str_replace('%ID%', $idMaterial, $queryRequirement);
                //die();

                while ($rowRequirement = mysqli_fetch_assoc($queryRequirementResult)) {
                    $row = new Zend\Text\Table\Row();

                    // первый ряд с rowspan
                    if ($numRequirement == 0) {
                        // echo " add first row for material numRequirements:$numRequirements \r\n";

                        $appTradeMark = '';
                        $appCountry = '';
                        $appTMC = '';
                        if ($bFillApp) {
                            $appTradeMark = getAppTradeMark($rowMaterial);
                            $appCountry = getAppCountry($rowMaterial);
                            $appTradeMark <> "" ? $appTMC = " ($appTradeMark, $appCountry)" : $appTMC = " ($appCountry)";

                            $appTradeMark == "" && $appCountry == "" ? $appTMC = "" : "";
                        }

                        $row->appendColumn(new Zend\Text\Table\Column($numMaterialRandom, null, null, null, $numRequirements));
                        $row->appendColumn(new Zend\Text\Table\Column($titleMaterial . $appTMC, null, null, null, $numRequirements));
                    }

                    $paramTitle = cleanString($rowRequirement['ParamTitle']);
                    $idParameter = $rowRequirement['ParameterId'];
                    $unitTitle = trim($rowRequirement['UnitShortTitle']);
                    $unitFullTitle = trim($rowRequirement['UnitFullTitle']);

                    // $rowRequirement['isRange'] ? $rangeModifer = '*' : $rangeModifer = '';
                    $rowRequirement['OrderRange'] ? $rangeIndex = 1 : $rangeIndex = 0;

                    $instruction[$titleMaterial][$rangeIndex][] = $paramTitle;
                    //$instruction[] = $nextInstruction;

                    $appValue = '';
                    if ($bFillApp) {
                        $appValue = getAppValue($rowRequirement, $bConvertAppNumbersToWord);
                    }

                    $minValue = getMinValue($rowRequirement, true);
                    $maxValue = getMaxValue($rowRequirement, true);
                    $fixedValue = getFixedValue($rowRequirement);

                    if (!$bFillApp) {
                        if ($numRequirement == 0) {
                            $row->appendColumn(new Zend\Text\Table\Column($paramTitle));
                            $row->appendColumn(new Zend\Text\Table\Column($minValue));
                            $row->appendColumn(new Zend\Text\Table\Column($maxValue));
                            $row->appendColumn(new Zend\Text\Table\Column($fixedValue));
                            $row->appendColumn(new Zend\Text\Table\Column("", null, null, null, $numRequirements));
                            $row->appendColumn(new Zend\Text\Table\Column($unitFullTitle));
                        } else {
                            $row->appendColumn(new Zend\Text\Table\Column($paramTitle));
                            $row->appendColumn(new Zend\Text\Table\Column($minValue));
                            $row->appendColumn(new Zend\Text\Table\Column($maxValue));
                            $row->appendColumn(new Zend\Text\Table\Column($fixedValue));
                            $row->appendColumn(new Zend\Text\Table\Column($unitFullTitle));
                        }
                    } else {
                        $row->appendColumn(new Zend\Text\Table\Column($paramTitle));
                        $row->appendColumn(new Zend\Text\Table\Column($minValue));
                        $row->appendColumn(new Zend\Text\Table\Column($maxValue));
                        $row->appendColumn(new Zend\Text\Table\Column($fixedValue));
                        $row->appendColumn(new Zend\Text\Table\Column($appValue));
                        $row->appendColumn(new Zend\Text\Table\Column($unitFullTitle));
                    }

                    $table->appendRow($row);

                    $numRequirement++;
                }
            }
            $numMaterial++;

        }

        $strResult .= $table;

        if (!$bFillApp) {
            $strResult .= "* - содержит требования к фиксированному значению показателя (в виде одного или нескольких значений, которым должно соответствовать значения показателя или перечисления допустимых значений показателя, из которых необходимо сделать выбор). В случае требования к фракции является указанием на конкретные фракции в виде интервалов значений.\r\n";
            $strResult .= "** - данные поля являются рекомендуемыми для заполнения участником закупки при использовании предложенной формы и не содержат требований заказчика к показателям/характеристикам товаров/материалов.\r\n";
            $strResult .= "\r\n";
            $strResult .= generateInstruction($instruction, $bGenerateParagraphs, $bGenerateLists) . " ";
        }
        break;

    // ----------------------------------------------------- //
    // Таблица c rowspan и 1 колонкой требований
    // ----------------------------------------------------- //
    case 5:

        $table = new Zend\Text\Table\Table([
            'columnWidths' => [5, 15, 12, 15, 15, 15, 10]
        ]);

        $instruction = array();

        $row = new Zend\Text\Table\Row();
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_num']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_material_title']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_parameter_num']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_parameter_title']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_requirements']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_proposal_values']));
        $row->appendColumn(new Zend\Text\Table\Column($language['table']['header']['col_unit']));
        $table->appendRow($row);

        $queryMaterialsResult = mysqli_query($dbConnect, $queryMaterials);
        $numMaterial = 0;
        while ($rowMaterial = mysqli_fetch_assoc($queryMaterialsResult)) {

            $orderMaterial = $rowMaterial['Order'];
            $idMaterial = $rowMaterial['MaterialId'];
            $titleMaterial = trim($rowMaterial['MaterialTitle']);
            $numMaterialRandom = $rowMaterial['NumRandom'];

            // echo "next material:  $orderMaterial. $titleMaterial\r\n";

            if ($idMaterial > 0) {
                $queryRequirementResult = mysqli_query($dbConnect, str_replace('%ID%', $idMaterial, $queryRequirement));
                $numRequirements = mysqli_num_rows($queryRequirementResult);
                $numRequirement = 0;

                while ($rowRequirement = mysqli_fetch_assoc($queryRequirementResult)) {
                    $row = new Zend\Text\Table\Row();

                    // первый ряд с rowspan
                    if ($numRequirement == 0) {
                        // echo " add first row for material numRequirements:$numRequirements \r\n";

                        $appTradeMark = '';
                        $appCountry = '';
                        $appTMC = '';
                        if ($bFillApp) {
                            $appTradeMark = getAppTradeMark($rowMaterial);
                            $appCountry = getAppCountry($rowMaterial);
                            $appTradeMark <> "" ? $appTMC = " ($appTradeMark, $appCountry)" : $appTMC = " ($appCountry)";

                            $appTradeMark == "" && $appCountry == "" ? $appTMC = "" : "";
                        }

                        $row->appendColumn(new Zend\Text\Table\Column($numMaterialRandom, null, null, null, $numRequirements));
                        $row->appendColumn(new Zend\Text\Table\Column($titleMaterial . $appTMC, null, null, null, $numRequirements));
                    }

                    $paramTitle = cleanString($rowRequirement['ParamTitle']);
                    $paramNum = $rowRequirement['ParamNum'];
                    $idParameter = $rowRequirement['ParameterId'];
                    $unitTitle = trim($rowRequirement['UnitShortTitle']);
                    $unitFullTitle = trim($rowRequirement['UnitFullTitle']);

                    // $rowRequirement['OrderRange'] ? $rangeIndex = 1 : $rangeIndex = 0;
                    $rangeIndex = (int)$rowRequirement['OrderRange'];

                    if ($bInstructionReqNames) {
                        $instruction[$titleMaterial][$rangeIndex][] = $paramTitle;
                    } else {
                        $instruction[$titleMaterial][$rangeIndex][] = $paramNum; // $numMaterialRandom.".".$paramNum;
                    }

                    $appValue = '';
                    if ($bFillApp) {
                        $appValue = getAppValue($rowRequirement, $bConvertAppNumbersToWord);
                    }

                    $minValue = getMinValue($rowRequirement, true);
                    $maxValue = getMaxValue($rowRequirement, true);
                    $fixedValue = getFixedValue($rowRequirement);

                    // $paramNumFinal = $numMaterialRandom.".".$paramNum;
                    $paramNumFinal = $paramNum;

                    if (!$bFillApp) {
                        if ($numRequirement == 0) {
                            $row->appendColumn(new Zend\Text\Table\Column($paramNumFinal, Zend\Text\Table\Column::ALIGN_RIGHT));
                            $row->appendColumn(new Zend\Text\Table\Column($paramTitle));
                            $row->appendColumn(new Zend\Text\Table\Column(getValue($rowRequirement, true, $bConvertReqNumbersToWord)));
                            $row->appendColumn(new Zend\Text\Table\Column("", null, null, null, $numRequirements));
                            $row->appendColumn(new Zend\Text\Table\Column($unitTitle));
                        } else {
                            $row->appendColumn(new Zend\Text\Table\Column($paramNumFinal, Zend\Text\Table\Column::ALIGN_RIGHT));
                            $row->appendColumn(new Zend\Text\Table\Column($paramTitle));
                            $row->appendColumn(new Zend\Text\Table\Column(getValue($rowRequirement, true, $bConvertReqNumbersToWord)));
                            $row->appendColumn(new Zend\Text\Table\Column($unitTitle));
                        }
                    } else {
                        $row->appendColumn(new Zend\Text\Table\Column($paramNumFinal, Zend\Text\Table\Column::ALIGN_RIGHT));
                        $row->appendColumn(new Zend\Text\Table\Column($paramTitle));
                        $row->appendColumn(new Zend\Text\Table\Column(getValue($rowRequirement, true, $bConvertReqNumbersToWord)));
                        $row->appendColumn(new Zend\Text\Table\Column($appValue));
                        $row->appendColumn(new Zend\Text\Table\Column($unitTitle));
                    }

                    $table->appendRow($row);

                    $numRequirement++;
                }
            }
            $numMaterial++;

        }

        $strResult .= $table;

        if (!$bFillApp) {
            $strResult .= "* - содержит требования к фиксированному значению показателя (в виде одного или нескольких значений, которым должно соответствовать значения показателя или перечисления допустимых значений показателя, из которых необходимо сделать выбор). В случае требования к фракции является указанием на конкретные фракции в виде интервалов значений.\r\n";
            $strResult .= "** - данные поля являются рекомендуемыми для заполнения участником закупки при использовании предложенной формы и не содержат требований заказчика к показателям/характеристикам товаров/материалов.\r\n";
            $strResult .= "\r\n";
            $strResult .= generateInstruction($instruction, $bGenerateParagraphs, $bGenerateLists, $bInstructionReqNames) . " ";
        }
        break;
    
}

// mysqli_close($dbConnect);
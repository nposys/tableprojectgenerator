CREATE DATABASE  IF NOT EXISTS `project` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `project`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: project
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Checkpoint`
--

DROP TABLE IF EXISTS `Checkpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Checkpoint` (
  `CheckpointId` int(11) NOT NULL AUTO_INCREMENT,
  `DateTime` datetime DEFAULT NULL,
  `ProjectId` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `NumCheckpoint` int(11) DEFAULT NULL,
  `Version` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`CheckpointId`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CommonProjectMaterial`
--

DROP TABLE IF EXISTS `CommonProjectMaterial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommonProjectMaterial` (
  `MaterialId` int(11) DEFAULT NULL,
  `ProjectId` int(11) DEFAULT NULL,
  `Order` int(11) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CommonProjectParameter`
--

DROP TABLE IF EXISTS `CommonProjectParameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommonProjectParameter` (
  `ParameterId` int(11) DEFAULT NULL,
  `ProjectId` int(11) DEFAULT NULL,
  `Order` int(11) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Item`
--

DROP TABLE IF EXISTS `Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MinRange` text,
  `MaxRange` text,
  `isRange` tinyint(1) DEFAULT NULL,
  `FixedValue` mediumtext,
  `isMinInclude` tinyint(1) DEFAULT NULL,
  `isMaxInclude` tinyint(1) DEFAULT NULL,
  `Comment` mediumtext,
  `MaterialId` int(11) NOT NULL,
  `ParameterId` int(11) NOT NULL,
  `UnitId` int(11) DEFAULT NULL,
  `NegativeAssert` tinyint(1) NOT NULL DEFAULT '0',
  `OrderValue` text,
  `ProjectId` int(11) DEFAULT NULL,
  `Additional` text,
  `sTradeMarkId` int(11) DEFAULT NULL,
  `sCountryId` int(11) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '1',
  `OrderRange` tinyint(4) NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(4) DEFAULT '0',
  `GroupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Item_Material_Id_fk` (`MaterialId`),
  KEY `Item_Parameter_Id_fk` (`ParameterId`),
  KEY `Item_Unit_Id_fk` (`UnitId`),
  CONSTRAINT `Item_Material_Id_fk` FOREIGN KEY (`MaterialId`) REFERENCES `Material` (`Id`),
  CONSTRAINT `Item_Parameter_Id_fk` FOREIGN KEY (`ParameterId`) REFERENCES `Parameter` (`Id`),
  CONSTRAINT `Item_Unit_Id_fk` FOREIGN KEY (`UnitId`) REFERENCES `Unit` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=80502 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ItemGroup`
--

DROP TABLE IF EXISTS `ItemGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ItemGroup` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectId` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5689 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Material`
--

DROP TABLE IF EXISTS `Material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Material` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` mediumtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=84351 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parameter`
--

DROP TABLE IF EXISTS `Parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parameter` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=83523 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Project`
--

DROP TABLE IF EXISTS `Project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Project` (
  `ProjectId` int(11) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ProjectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SortOrder`
--

DROP TABLE IF EXISTS `SortOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SortOrder` (
  `idSortOrder` int(11) NOT NULL AUTO_INCREMENT,
  `SortType` int(11) DEFAULT NULL,
  `MaterialId` int(11) DEFAULT NULL,
  `ParameterId` int(11) DEFAULT NULL,
  `NumOriginal` int(11) DEFAULT NULL,
  `NumRandom` int(11) DEFAULT NULL,
  `CheckpointId` int(11) DEFAULT NULL,
  `ProjectId` int(11) DEFAULT NULL,
  `Version` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idSortOrder`),
  UNIQUE KEY `idSortOrder_UNIQUE` (`idSortOrder`)
) ENGINE=InnoDB AUTO_INCREMENT=54787 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Unit`
--

DROP TABLE IF EXISTS `Unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Unit` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FullTitle` mediumtext,
  `ShortTitle` mediumtext,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=65639 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_RawCatalog`
--

DROP TABLE IF EXISTS `_RawCatalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_RawCatalog` (
  `MaterialTitle` text,
  `TradeMark` text,
  `Country` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_RawData`
--

DROP TABLE IF EXISTS `_RawData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_RawData` (
  `MaterialTitle` mediumtext,
  `ParameterTitle` mediumtext,
  `MinInclude` mediumtext,
  `Min` mediumtext,
  `MaxInclude` mediumtext,
  `Max` mediumtext,
  `isRange` mediumtext,
  `FixedValue` mediumtext,
  `isNo` mediumtext,
  `Unit` mediumtext,
  `ShortUnit` mediumtext,
  `Comment` mediumtext,
  `AppValue` text,
  `Additional` text,
  `OrderRange` text,
  `GroupTitle` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_RawData_dev`
--

DROP TABLE IF EXISTS `_RawData_dev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_RawData_dev` (
  `MaterialTitle` mediumtext,
  `ParameterTitle` mediumtext,
  `MinInclude` mediumtext,
  `Min` mediumtext,
  `MaxInclude` mediumtext,
  `Max` mediumtext,
  `isRange` mediumtext,
  `FixedValue` mediumtext,
  `isNo` mediumtext,
  `Unit` mediumtext,
  `ShortUnit` mediumtext,
  `Comment` mediumtext,
  `AppValue` text,
  `Additional` text,
  `OrderRange` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sCountry`
--

DROP TABLE IF EXISTS `sCountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sCountry` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sTradeMark`
--

DROP TABLE IF EXISTS `sTradeMark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sTradeMark` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(127) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'project'
--

--
-- Dumping routines for database 'project'
--
/*!50003 DROP PROCEDURE IF EXISTS `delete_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_project`(project INTEGER, version INTEGER)
BEGIN
	IF version = 0 
    THEN 
	   DELETE FROM CommonProjectParameter WHERE ProjectId = project;
	   DELETE FROM CommonProjectMaterial WHERE ProjectId = project;
	   DELETE FROM SortOrder WHERE ProjectId = project;
	   DELETE FROM Checkpoint WHERE ProjectId = project;
	   DELETE FROM Item WHERE ProjectId = project;
    ELSE
	   DELETE FROM CommonProjectParameter WHERE ProjectId = project AND Version = version;
	   DELETE FROM CommonProjectMaterial WHERE ProjectId = project AND Version = version;
	   DELETE FROM SortOrder WHERE ProjectId = project;
	   DELETE FROM Checkpoint WHERE ProjectId = project;
	   DELETE FROM Item WHERE ProjectId = project AND Version = version;    
    END IF;

  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `normalize_v2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `normalize_v2`(project INTEGER, version INTEGER)
BEGIN

    INSERT INTO Unit (FullTitle, ShortTitle)  SELECT Unit, ShortUnit FROM `_RawData`;
    INSERT INTO Parameter (Title) SELECT ParameterTitle FROM `_RawData`;
    INSERT INTO Material (Title) SELECT MaterialTitle FROM `_RawData`;
    INSERT INTO ItemGroup (ProjectId, Title) SELECT project, GroupTitle FROM `_RawData`;

    CREATE TEMPORARY TABLE `item_group_temp`
    as  (
      SELECT min(Id) as id
      FROM `ItemGroup`
      WHERE ProjectId = project AND Title <> ''
      GROUP BY Title
    );
    
    CREATE TEMPORARY TABLE `unit_temp`
    as  (
      SELECT min(Id) as id
      FROM `Unit`
      GROUP BY FullTitle
    );

    CREATE TEMPORARY TABLE `parameter_temp`
    as  (
      SELECT min(Id) as id
      FROM `Parameter`
      GROUP BY Title
    );

    CREATE TEMPORARY TABLE `material_temp`
    as  (
      SELECT min(Id) as id
      FROM `Material`
      GROUP BY Title
    );

    DELETE from `Material`
    WHERE Id not in (
      SELECT id FROM material_temp
    );

    DELETE from `Parameter`
    WHERE Id not in (
      SELECT id FROM parameter_temp
    );

    DELETE from `Unit`
    WHERE Id not in (
      SELECT id FROM unit_temp
    );
    
	DELETE from `ItemGroup`
    WHERE ProjectId = project and Id not in (
      SELECT id FROM item_group_temp
    );

    INSERT INTO Item (MinRange, MaxRange, FixedValue, isMinInclude, isMaxInclude, isRange,
                      MaterialId, ParameterId, ProjectId, UnitId, NegativeAssert, OrderValue, Additional, Version, OrderRange, GroupId)
      SELECT
        raw.Min
        , raw.Max
        , raw.FixedValue
        , CASE WHEN raw.MinInclude = '+' THEN 1 ELSE 0 END
        , CASE WHEN raw.MaxInclude = '+' THEN 1 ELSE 0 END
        , CASE WHEN raw.isRange = '+' THEN 1 ELSE 0 END
        , (SELECT Id FROM Material WHERE Title = raw.MaterialTitle)
        , (SELECT Id FROM Parameter WHERE Title = raw.ParameterTitle)
        , project
        , (SELECT CASE WHEN Id is NULL THEN 0 ELSE Id END FROM Unit WHERE FullTitle = raw.Unit)
        , CASE WHEN raw.isNo = '+' THEN 1 ELSE 0 END
        , raw.AppValue
        , raw.Additional
        , version
        , CASE WHEN raw.OrderRange = '1' OR raw.OrderRange = '+'
			THEN 1 
            ELSE 
				CASE WHEN raw.OrderRange = '2' 
					THEN 2 
					ELSE 
						0 
					END 
			END
        , (SELECT CASE WHEN Id is NULL THEN 0 ELSE Id END FROM ItemGroup WHERE ProjectId = project and Title = raw.GroupTitle)
      FROM `_RawData`as raw;

    SET @row_number = 0;
    INSERT INTO CommonProjectParameter (ParameterId, ProjectId, `Order`, Version)
      SELECT Id, project, (@row_number:=@row_number + 1) as num, version FROM Parameter WHERE Title IN (
        SELECT ParameterTitle
        FROM `_RawData`
        GROUP BY ParameterTitle
      );

    SET @row_number2 = 0;
    INSERT INTO CommonProjectMaterial (MaterialId, ProjectId, `Order`, Version)
      SELECT Id, project, (@row_number2:=@row_number2 + 1) as num, version FROM Material WHERE Title IN (
        SELECT MaterialTitle
        FROM `_RawData`
        GROUP BY MaterialTitle
      );

    DROP TEMPORARY TABLE `unit_temp`;
    DROP TEMPORARY TABLE `parameter_temp`;
    DROP TEMPORARY TABLE `material_temp`;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-03 21:55:00

<?php
/**
 * Created by PhpStorm.
 * User: otkidyshev
 * Date: 10.07.17
 * Time: 11:41
 */

require_once 'config.php';
require_once 'functions.php';
$project = (int)trim($_REQUEST['project']);
$ver = getMaxVersion($mysqli, $project);
$error = true;

if (is_uploaded_file($_FILES["file"]["tmp_name"])) {
    if (!checkProject($mysqli, $project, $ver) || !$project) {
        header('Location: ' .HOST. '/index.php?error=load&error_type=check_project');
        exit;
    }
    // Если файл загружен успешно, перемещаем его
    // из временной директории в конечную
    move_uploaded_file
    (
        $_FILES["file"]["tmp_name"],
        TMP_DIR . '/' . $_FILES["file"]["name"]
    );
    $error = false;

    if (loadFile($mysqli, TMP_DIR . '/' . $_FILES["file"]["name"], $project, $ver)) {
        header('Location: ' .HOST. '/index.php?success=project');
        exit;
    }
}

if (is_uploaded_file($_FILES["catalog"]["tmp_name"])) {
    move_uploaded_file
    (
        $_FILES["catalog"]["tmp_name"],
        TMP_DIR . '/' . $_FILES["catalog"]["name"]
    );
    $error = false;

    if (loadCatalog($mysqli, TMP_DIR . '/' . $_FILES["catalog"]["name"], $project, $ver)) {
        header('Location: ' .HOST. '/index.php?success=catalog');
        exit;
    }
}

if ($error) {
    header('Location: ' .HOST. '/index.php?error=load&error_type=unknown_error');
} else {
    die ('что-то пошло не так с загрузкой');
}
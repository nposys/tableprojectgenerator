<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf6b3937f7cb08e104834c2935578e56a
{
    public static $prefixLengthsPsr4 = array (
        'Z' => 
        array (
            'Zend\\Text\\' => 10,
            'Zend\\Stdlib\\' => 12,
            'Zend\\ServiceManager\\' => 20,
        ),
        'P' => 
        array (
            'Psr\\Container\\' => 14,
        ),
        'I' => 
        array (
            'Interop\\Container\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Zend\\Text\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-text/src',
        ),
        'Zend\\Stdlib\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-stdlib/src',
        ),
        'Zend\\ServiceManager\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-servicemanager/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'Interop\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/container-interop/container-interop/src/Interop/Container',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf6b3937f7cb08e104834c2935578e56a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf6b3937f7cb08e104834c2935578e56a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}

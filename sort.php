<?php
ini_set('max_execution_time', 900);

require 'config.php';
require 'functions.php';

$bDoSortMaterials = true;
$bDoSortParameters = true;

isset($_GET['ProjectId']) ? $projectId = (int)($_GET['ProjectId']) : $projectId = "";
isset($_GET['RandomSortMat']) ? $randomSortMat = (int)($_GET['RandomSortMat']) : $randomSortMat = "";
isset($_GET['RandomSortReq']) ? $randomSortReq = (int)($_GET['RandomSortReq']) : $randomSortReq = "";

if ($projectId == "") {
    die('error get projectID');
}

echo "projectId:$projectId<br>\r\n";

$checkpointNum = getMaxCheckpointNum($mysqli, $projectId) + 1;
echo "new checkpointNum:$checkpointNum<br>\r\n";

insertCheckpoint($mysqli, $projectId, $checkpointNum);
$checkpointId = getCheckpointIdByNum($mysqli, $projectId, $checkpointNum);
echo "new checkpointId:$checkpointId<br>\r\n";

$queryMaterials = "
SELECT DISTINCT
    cpm.Order,
    cpm.MaterialId,
    m.Title as MaterialTitle
FROM
    project.CommonProjectMaterial as cpm
        left join project.Material as m on m.Id = cpm.MaterialId
WHERE
    cpm.ProjectId = $projectId
    and cpm.Version = (SELECT max(Version) from project.CommonProjectMaterial where ProjectId = $projectId)
";

// ======================================================================
// получаем список материалов и создаем случайную сортировку
$materials = [];
$randomNums = [];
$queryMaterialsResult = $mysqli->query($queryMaterials);
$i = 1;
while ($rowMaterial = $queryMaterialsResult->fetch_assoc()) {

    $orderMaterial = $rowMaterial['Order'];
    $idMaterial = $rowMaterial['MaterialId'];
    $titleMaterial = $rowMaterial['MaterialTitle'];

    if ($randomSortMat === 1) {
        $randomNum = mt_rand (1, mt_getrandmax());
    } else {
        $randomNum = $i;
    }

    $nextPosition = array(
        'idMaterial' => $idMaterial,
        'orderOriginal' => $orderMaterial,
        'orderRandom' => $randomNum
    );

    $materials[] = $nextPosition;
    $randomNums[] = $nextPosition;
    $i++;
}

usort($randomNums, function ($a, $b) {
    return $b['orderRandom'] < $a['orderRandom'];
});

// ======================================================================
// сохраняем сортировку материалов в базе
if ($bDoSortMaterials) {
    $sortType = 1;

    foreach ($randomNums as $randomIndex => $nextPosition) {

        $idMaterial = $nextPosition['idMaterial'];
        $orderOriginal = $nextPosition['orderOriginal'];
        $orderRandom = $nextPosition['orderRandom'];

        $randomIndex = $randomIndex + 1;

        $queryInsertRandomSort = "
            INSERT INTO project.SortOrder
            (
                SortType,
                MaterialId,
                NumOriginal,
                NumRandom,
                CheckpointId,
                ProjectId
            )
            VALUES
            (
                $sortType,
                $idMaterial,
                $orderOriginal,
                $randomIndex,
                $checkpointId,
                $projectId
            )
                ";

        $mysqli->real_query($queryInsertRandomSort);
    }

} // end bDoSortMaterials

// ======================================================================
if ($bDoSortParameters) {
    $queryParameters = "
    SELECT DISTINCT
        i.MaterialId ,
        p.Id as ParametrId,
        p.Title as ParamTitle,
        m.Title as MaterialTitle
    FROM
        project.Item as i
          left join project.Parameter as p on p.Id = i.ParameterId
          left join project.Material as m on m.Id = i.MaterialId
    WHERE
        i.ProjectId = $projectId and
       i.MaterialId = %ID%
       and i.Version = (SELECT max(Version) from project.Item where ProjectId = $projectId)
       and i.IsDeleted <> 1
    ";

    $sortType = 2;

    // перебираем все материалы проекта
    foreach ($materials as $materialIndex => $nextMaterial) {
        $materialID = $nextMaterial['idMaterial'];
        $queryParametersResult = $mysqli->query(str_replace('%ID%', $materialID, $queryParameters));

        // по каждому материалу создаем случайную сортировку параметров
        $randomNums = [];
        $i = 1;
        while ($rowParameters = $queryParametersResult->fetch_assoc()) {
            $idMaterial = $rowParameters['MaterialId'];
            $idParameter = $rowParameters['ParametrId'];

            if ($randomSortReq === 1) {
                $randomNum = mt_rand (1, mt_getrandmax());
            } else {
                $randomNum = $i;
            }

            $nextPosition = array(
                'idMaterial' => $idMaterial,
                'idParameter' => $idParameter,
                'orderOriginal' => $i,
                'orderRandom' => $randomNum
            );

            $randomNums[] = $nextPosition;

            $i++;
        }

        usort($randomNums, function ($a, $b) {
            return $b['orderRandom'] < $a['orderRandom'];
        });

        // удаляем предыдущие сортировки
        // $queryDeleteRandomSort = "DELETE FROM project.SortOrder WHERE MaterialId=$materialID and ProjectId=$projectId and CheckpointId=$checkpointId and SortType=$sortType";
        // mysqli_real_query($dbConnect, $queryDeleteRandomSort);

        foreach ($randomNums as $randomIndex => $nextPosition) {

            $idMaterial = $nextPosition['idMaterial'];
            $idParameter = $nextPosition['idParameter'];
            $orderOriginal = $nextPosition['orderOriginal'];
            $orderRandom = $nextPosition['orderRandom'];

            $randomIndex = $randomIndex + 1;

            $queryInsertRandomSort = "
                INSERT INTO project.SortOrder
                (
                SortType,
                MaterialId,
                ParameterId,
                NumOriginal,
                NumRandom,
                CheckpointId,
                ProjectId)
                VALUES
                (
                $sortType,
                $idMaterial,
                $idParameter,
                $orderOriginal,
                $randomIndex,
                $checkpointId,
                $projectId
                )
                ";

            $mysqli->real_query($queryInsertRandomSort);
        }
    } // end foreach materials
} // end if($bDoSortParameters)

header('Location: ' .HOST. '/index.php?success=checkpoint&checkpoint_project='.$projectId.'&create_checkpoint='.$checkpointId.'&select_view_mode=');
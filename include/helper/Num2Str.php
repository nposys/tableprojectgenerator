<?php
global $config;
if (true) {
    $minus = "минус ";
    $zero = "ноль ";

    $_1_2[1]="одна ";
    $_1_2[2]="две ";

    $_1_19[1]="один ";
    $_1_19[2]="два ";
    $_1_19[3]="три ";
    $_1_19[4]="четыре ";
    $_1_19[5]="пять ";
    $_1_19[6]="шесть ";
    $_1_19[7]="семь ";
    $_1_19[8]="восемь ";
    $_1_19[9]="девять ";
    $_1_19[10]="десять ";

    $_1_19[11]="одиннадцать ";
    $_1_19[12]="двенадцать ";
    $_1_19[13]="тринадцать ";
    $_1_19[14]="четырнадцать ";
    $_1_19[15]="пятнадцать ";
    $_1_19[16]="шестнадцать ";
    $_1_19[17]="семнадцать ";
    $_1_19[18]="восемнадцать ";
    $_1_19[19]="девятнадцать ";

    $tens[2]="двадцать ";
    $tens[3]="тридцать ";
    $tens[4]="сорок ";
    $tens[5]="пятьдесят ";
    $tens[6]="шестьдесят ";
    $tens[7]="семьдесят ";
    $tens[8]="восемьдесят ";
    $tens[9]="девяносто ";

    $hund[1]="сто ";
    $hund[2]="двести ";
    $hund[3]="триста ";
    $hund[4]="четыреста ";
    $hund[5]="пятьсот ";
    $hund[6]="шестьсот ";
    $hund[7]="семьсот ";
    $hund[8]="восемьсот ";
    $hund[9]="девятьсот ";

    $nametho[1]="тысяча ";
    $nametho[2]="тысячи ";
    $nametho[3]="тысяч ";

    $namemil[1]="миллион ";
    $namemil[2]="миллиона ";
    $namemil[3]="миллионов ";

    $namemrd[1]="миллиард ";
    $namemrd[2]="миллиарда ";
    $namemrd[3]="миллиардов ";

    $female["tho"]=1;
    $female["mil"]=0;
    $female["mrd"]=0;
} elseif ($config["lang"]=="ukrainian") {
    $minus = "мінус ";
    $zero = "нуль ";

    $_1_2[1]="одна ";
    $_1_2[2]="дві ";

    $_1_19[1]="один ";
    $_1_19[2]="два ";
    $_1_19[3]="три ";
    $_1_19[4]="чотири ";
    $_1_19[5]="п'ять ";
    $_1_19[6]="шість ";
    $_1_19[7]="сім ";
    $_1_19[8]="вісім ";
    $_1_19[9]="дев'ять ";
    $_1_19[10]="десять ";

    $_1_19[11]="одинадцять ";
    $_1_19[12]="дванадцять ";
    $_1_19[13]="тринадцять ";
    $_1_19[14]="чотирнадцять ";
    $_1_19[15]="п'ятнадцять ";
    $_1_19[16]="шістнадцять ";
    $_1_19[17]="сімнадцять ";
    $_1_19[18]="вісімнадцять ";
    $_1_19[19]="дев'ятнадцять ";

    $tens[2]="двадцять ";
    $tens[3]="тридцять ";
    $tens[4]="сорок ";
    $tens[5]="п'ятдесят ";
    $tens[6]="шістдесят ";
    $tens[7]="сімдесят ";

    $tens[8]="вісімдесят ";
    $tens[9]="дев'яносто ";

    $hund[1]="сто ";
    $hund[2]="двісті ";
    $hund[3]="триста ";
    $hund[4]="чотириста ";
    $hund[5]="п'ятсот ";
    $hund[6]="шістсот ";
    $hund[7]="сімсот ";
    $hund[8]="вісімсот ";
    $hund[9]="дев'ятсот ";

    $nametho[1]="тисяча ";
    $nametho[2]="тисячі ";
    $nametho[3]="тисяч ";

    $namemil[1]="мільйон ";
    $namemil[2]="мільйона ";
    $namemil[3]="мільйонів ";

    $namemrd[1]="мільярд ";
    $namemrd[2]="мільярди ";
    $namemrd[3]="мільярдів ";

    $female["tho"]=1;
    $female["mil"]=0;
    $female["mrd"]=0;
} elseif ($config["lang"]=="polish") {
    $minus = "minus ";
    $zero = "zero ";

    $_1_2[1]="jeden ";
    $_1_2[2]="dwa ";

    $_1_19[1]="jeden ";
    $_1_19[2]="dwa ";
    $_1_19[3]="trzy";
    $_1_19[4]="cztery ";
    $_1_19[5]="pięć ";
    $_1_19[6]="sześć ";
    $_1_19[7]="siedem ";
    $_1_19[8]="osiem ";
    $_1_19[9]="dziewięć ";
    $_1_19[10]="dziesięć ";

    $_1_19[11]="jedenaście ";
    $_1_19[12]="dwanaście ";
    $_1_19[13]="trzynaście ";
    $_1_19[14]="czternaście ";
    $_1_19[15]="piętnaście ";
    $_1_19[16]="szesnaście ";
    $_1_19[17]="siedemnaście ";
    $_1_19[18]="osiemnaście ";
    $_1_19[19]="dziewiętnaście ";

    $tens[2]="dwadzieścia ";
    $tens[3]="trzydzieści ";
    $tens[4]="czterdzieści ";
    $tens[5]="pięćdziesiąt ";
    $tens[6]="sześćdziesiąt ";
    $tens[7]="siedemdziesiąt ";
    $tens[8]="osiemdziesiąt ";
    $tens[9]="dziewięćdziesiąt ";

    $hund[1]="sto ";
    $hund[2]="dwieście ";
    $hund[3]="trzysta ";
    $hund[4]="czterysta ";
    $hund[5]="pięćset ";
    $hund[6]="sześćset ";
    $hund[7]="siedemset ";
    $hund[8]="osiemset ";
    $hund[9]="dziewięćset  ";

    $nametho[1]="tysiąc ";
    $nametho[2]="tysiące ";
    $nametho[3]="tysięcy ";

    $namemil[1]="milion ";
    $namemil[2]="miliony ";
    $namemil[3]="milionów ";

    $namemrd[1]="miliard ";
    $namemrd[2]="miliardy ";
    $namemrd[3]="miliardów ";

    $female["tho"]=1;
    $female["mil"]=0;
    $female["mrd"]=0;
} else {
    $minus = "minus ";
    $zero = "zero ";

    $_1_2[1]="one ";
    $_1_2[2]="two ";

    $_1_19[1]="one ";
    $_1_19[2]="two ";
    $_1_19[3] = "three ";
    $_1_19[4] = "four ";
    $_1_19[5] = "five ";
    $_1_19[6] = "six ";
    $_1_19[7] = "seven ";
    $_1_19[8] = "eight ";
    $_1_19[9] = "nine ";
    $_1_19[10] = "ten ";

    $_1_19[11] = "eleven ";
    $_1_19[12] = "twelve ";
    $_1_19[13] = "thirteen ";
    $_1_19[14] = "fourteen ";
    $_1_19[15] = "fifteen ";
    $_1_19[16] = "sixteen ";
    $_1_19[17] = "seventeen ";
    $_1_19[18] = "eighteen ";
    $_1_19[19] = "nineteen ";

    $tens[2] = "twenty ";
    $tens[3] = "thirty ";
    $tens[4] = "forty ";
    $tens[5] = "fifty ";
    $tens[6] = "sixty ";
    $tens[7] = "seventy ";
    $tens[8] = "eighty ";
    $tens[9] = "ninety ";

    $hund[1] = "hundred ";
    $hund[2] = "two hundred ";
    $hund[3] = "three hundred ";
    $hund[4] = "four hundred ";
    $hund[5] = "five hundred ";
    $hund[6] = "six hundred ";
    $hund[7] = "seven hundred ";
    $hund[8] = "eight hundred ";
    $hund[9] = "nine hundred ";

    $nametho[1] = "thousand ";
    $nametho[2] = "thousands ";
    $nametho[3] = "thousand ";

    $namemil[1] = "million ";
    $namemil[2] = "million ";
    $namemil[3] = "million ";

    $namemrd[1] = "billion ";
    $namemrd[2] = "billion ";
    $namemrd[3] = "billion ";

    $female["tho"]=1;
    $female["mil"]=0;
    $female["mrd"]=0;
}


if (!function_exists("num2str")) {

    function semantic($i, &$words, &$fem, $f) {
        global $_1_2, $_1_19, $tens, $hund;
        $words="";
        $fl=0;
        if($i >= 100){
            $jkl = intval($i / 100);
            $words.=$hund[$jkl];
            $i%=100;
        }
        if($i >= 20){
            $jkl = intval($i / 10);
            $words.=$tens[$jkl];
            $i%=10;
            $fl=1;
        }
        switch($i){
            case 1: $fem=1; break;
            case 2:
            case 3:
            case 4: $fem=2; break;
            default: $fem=3; break;
        }
        if( $i ){
            if( $i < 3 && $f == 1 ){
                $words.=$_1_2[$i];
            }
            else {
                $words.=$_1_19[$i];
            }
        }
    }


    function num2str($num, $currency, $cents, $caps, $female_cur=0, $female_cents=0) {
        global $nametho, $namemil, $namemrd, $minus, $zero, $female, $lang;
        if (!$currency) $currency=$lang["currency"];
        if (!$cents)    $cents=$lang["currency_cents"];
        if ($currency=="dollars") {
            $namecur[1]="dollar ";
            $namecur[2]="dollars ";
            $namecur[3]="dollars ";
            $female["cur"]=0;
        } elseif ($currency=="рубли") {
            $namecur[1]="рубль ";
            $namecur[2]="рубля ";
            $namecur[3]="рублей ";
            $female["cur"]=0;
        } elseif ($currency=="гривні") {
            $namecur[1]="гривня ";
            $namecur[2]="гривні ";
            $namecur[3]="гривень ";
            $female["cur"]=1;
        } elseif ($currency=="Złote") {
            $namecur[1]="złoty ";
            $namecur[2]="złote ";
            $namecur[3]="złotych ";
            $female["cur"]=1;
        } elseif ($currency!="none") {
            $namecur[1]=$currency." ";
            $namecur[2]=$currency." ";
            $namecur[3]=$currency." ";
            $female["cur"]=$female_cur;
        }

        if ($cents=="cents") {
            $namecents[1]="cent";
            $namecents[2]="cents";
            $namecents[3]="cents";
            $female["cents"]=1;
        }elseif (!$cents or $cents=="копейки") {
            $namecents[1]="копейка";
            $namecents[2]="копейки";
            $namecents[3]="копеек";
            $female["cents"]=1;
        } elseif ($cents=="копійки") {
            $namecents[1]="копійка";
            $namecents[2]="копійки";
            $namecents[3]="копійок";
            $female["cents"]=1;
        } elseif ($cents=="grosze") {
            $namecents[1]="grosz ";
            $namecents[2]="grosza ";
            $namecents[3]="groszy ";
            $female["cents"]=1;
        } elseif ($cents!="none") {
            $namecents[1]=$cents;
            $namecents[2]=$cents;
            $namecents[3]=$cents;
            $female["cents"]=$female_cents;
        }

        $rub = abs(intval($num));
        if (strpos($num,"."))
        {
            $kop = substr($num,strpos($num,".")+1);
            if (strlen($kop)==1) $kop.="0";
            $kop=intval($kop);
        } else $kop = 0;


        $s = "";
        if ($rub>=1000000000) {
            $many=0;
            semantic(intval($rub / 1000000000),$s1,$many,$female["mrd"]);
            $s.=$s1.$namemrd[$many];
            $rub%=1000000000;
            if ($rub==0) $s.=$namecur[3];
        }

        if ($rub >= 1000000) {
            $many=0;
            semantic(intval($rub / 1000000),$s1,$many,$female["mil"]);
            $s.=$s1.$namemil[$many];
            $rub%=1000000;
            if ($rub==0) $s.=$namecur[3];
        }

        if ($rub >= 1000) {
            $many=0;
            semantic(intval($rub / 1000),$s1,$many,$female["tho"]);
            $s.=$s1.$nametho[$many];
            $rub%=1000;
            if ($rub==0) $s.=$namecur[3];
        }

        if ($rub != 0) {
            $many=0;
            semantic($rub,$s1,$many,$female["cur"]);
            $s.=$s1.$namecur[$many];
        } elseif (!$s) {
            $s.=$zero.$namecur[3];
        }

        if ($cents!=="none") {
            $many=0;
            semantic($kop,$s1,$many,$female["cents"]);
            $s.=sprintf(" %02.0f ",$kop).$namecents[$many];
        }

        $s = trim($s);
        if ($num<0) $s = $minus.$s;
        if ($caps) $s = mb_strtoupper(mb_substr($s,0,1)).mb_substr($s,1);

        return $s;
    }

}

function num2str_fraction($num, $currency, $cents, $caps)
{

    $arr_words1 = ["целая","целых"];
    $arr_words2 = [ 0=>["десятая","сотая","тысячная","десятитысячная"]
                    ,1=>["десятых","сотых","тысячных","десятитысячных"]];


    $str_number = strval($num);
    if(strpos($str_number, ".")>0)
    {
        $str_array = explode(".",$str_number);

        $num1 = $str_array[0];
        $num2 = $str_array[1];

        $num1_len = strlen($num1) - 1;
        $num1_final = substr($num1, -1, 1);
        $num1_final2 = substr($num1, -2, 1);
        if($num1_final == "1" && ($num1_final2 == "0" || $num1_len==0))
        {
            $add_str_num1 = $arr_words1[0];
            $str_num1 = num2str($num1, $currency, $cents, $caps, 1);
        }
        else
        {
            $add_str_num1 = $arr_words1[1];
            $str_num1 = num2str($num1, $currency, $cents, $caps, 0);
        }

        $num2_len = strlen($num2) - 1;
        $num2_final = substr($num2, -1, 1);
        $num2_final2 = substr($num2, -2, 1);
        if($num2_final == "1" && ($num2_final2 == "0" || $num2_len==0))
        {
            $add_str_num2 = $arr_words2[0][$num2_len];
            $str_num2 = num2str($num2, $currency, $cents, $caps, 1);
        }
        else
        {
            $add_str_num2 = $arr_words2[1][$num2_len];
            $str_num2 = num2str($num2, $currency, $cents, $caps);
        }



        $result = $str_num1." ".$add_str_num1." ".$str_num2." ".$add_str_num2;

    }
    else
    {
        $result = num2str($num, $currency, $cents, $caps);
    }

    return $result;
}

?>
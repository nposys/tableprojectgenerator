<?php
/**
 * Copyright (c) 2017.  SPC "System"
 */

namespace module\common\helper\FloatToWord;


use module\common\helper\FloatToWord\Locale\Language;
use module\common\helper\FloatToWord\Locale\Ru;

class FloatToWord
{

	private $locale;
	private $localeName;

	public function __construct($locale = 'Ru')
	{
		$this->localeName = $locale;
		switch ($locale) {
			case 'Ru':
				$this->locale = Ru::setDefault();
				break;
			default:
				throw new FloatToWordException('Locale is not supported');
				break;
		}
	}


	public function format(&$num, $int = false, $singular = 0) {
		if ($singular && $this->localeName == 'Ru') $this->locale = Ru::setPlural();
		list($integer, $fractional) = explode('.', sprintf("%017.4f", floatval($num)));
		$out = array();
		$transform = function ($digit, $unit) use (&$out) {
			if (intval($digit) > 0) {
				foreach (str_split($digit, 3) as $uk => $v) { // by 3 symbols
					if (!intval($v)) {
						continue;
					}
					$uk = sizeof($unit) - $uk - 1; // unit key
					list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
					// mega-logic
					$out[] = $this->locale->hundred[$i1]; # 1xx-9xx
					if ($i2 > 1) {
						$out[] = $this->locale->tens[$i2] . ' ' . $this->locale->ten[$i3];
					} # 20-99
					else {
						$out[] = $i2 > 0 ? $this->locale->a20[$i3] : $this->locale->ten[$i3];
					} # 10-19 | 1-9
					// units without rub & kop
					if ($uk > 1) {
						$out[] = self::morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
					}
				} //foreach
			} else {
				$out[] = $this->locale->nul;
			}
			$out[] = self::morph(intval($digit), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
		};
		$transform($integer, $this->locale->unitInteger + $this->locale->digits);
		$fractionalLength = strlen($fractional);
		$tmp = str_split($fractional, 1);
		$fractional = array();
		$matchEnd = false;
		for ($i = $fractionalLength - 1; $i >= 0; $i--) {
			if ($tmp[$i] != 0 || $matchEnd) {
				array_unshift($fractional, $tmp[$i]);
				$matchEnd = true;
			}
		}
		$fractionalLength = strlen(implode('', $fractional));
		$fractional = (int) implode('', $fractional);
		list($fractional, $garbage) = explode('.',sprintf("%015.2f", floatval($fractional)));
		$transform($fractional, $this->locale->unitFractional[$fractionalLength] + $this->locale->digits);

		return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
	}

	/**
	 * Склоняем словоформу
	 * @ author runcore
	 */
	private function morph($n, $f1, $f2, $f5) {
		$n = abs(intval($n)) % 100;
		if ($n>10 && $n<20) return $f5;
		$n = $n % 10;
		if ($n>1 && $n<5) return $f2;
		if ($n==1) return $f1;

		return $f5;
	}
}


<?php
require_once 'config.php';
require_once 'functions.php';
$baseFolder = ROOT . "/reports/";
$baseUrl = HOST;

$postVars = getRealPOST();
$after_create_checkpoint = false;

if ($_GET['error'] == 'load') {
    $message = '<div style="color: red">Невозможно загрузить файл.</div>';
}
if ($_GET['success'] == 'project'){
    $messageLoadProject = '<div style="color: green">Проект успешно загружен.</div>';
}
if ($_GET['success'] == 'catalog') {
    $messageLoadCatalog = '<div style="color: green">Каталог успешно загружен.</div>';
}
if ($_GET['success'] == 'checkpoint') {
    $messageCreateCheckpoint = '<div style="color: green">Контрольная точка успешно создана.</div>';
}

$projects = [];
$checkpoints = [];
$viewModes = [
    1 => 'Текстовый',
    2 => 'Таблица с colspan и 3 колонками требований',
    3 => 'Таблица с общей колонкой требований',
    4 => 'Таблица с rowspan и 3 колонками требований',
    5 => 'Таблица с rowspan и 1 колонкой требований'
];
$instructionViews = [
    1 => 'Простая инструкция',
    2 => 'Инструкция с параграфами без списков',
    3 => 'Инструкция с параграфами со списками',
    4 => 'Инструкция без параграфов без списков',
    5 => 'Инструкция без параграфов со списками',
];
$instructionModes = [
    1 => 'Материалы и требования названиями',
    2 => 'Требования цифрами'
];

$viewModesApp = ['Да', 'Нет'];
$viewModesNumbers = ['Текст', 'Числа'];

$querySelectProjects = "SELECT i.ProjectId, p.Title FROM project.Item AS i LEFT JOIN project.Project AS p ON p.ProjectId = i.ProjectId GROUP BY i.ProjectId ";
$resultProjects = $mysqli->query($querySelectProjects);
while ($rowProject = $resultProjects->fetch_array(MYSQLI_ASSOC)) {
    $nextProject = [];
    $nextProject['ProjectId'] = $rowProject['ProjectId'];
    $nextProject['ProjectTitle'] = ''; // $rowProject['Title'];

    $projects[] = $nextProject;

    $querySelectCheckpoints = "SELECT CheckpointId, NumCheckpoint, Datetime FROM project.Checkpoint WHERE ProjectId = " . $rowProject['ProjectId'];

    $resultCheckpoints = $mysqli->query($querySelectCheckpoints);
    while ($rowCheckpoint = $resultCheckpoints->fetch_array(MYSQLI_ASSOC)) {
        $checkpointId = (int)$rowCheckpoint['CheckpointId'];
        $numCheckpoint = $rowCheckpoint['NumCheckpoint'];
        $checkpointDatetime = $rowCheckpoint['Datetime'];

        $nextCheckpoint = ['id' => $checkpointId, 'num' => $numCheckpoint, 'datetime' => $checkpointDatetime];
        $checkpoints[$rowProject['ProjectId']][] = $nextCheckpoint;
    }
}

if (array_key_exists('select_view_mode', $postVars)) {
    $select_view_mode = $postVars['select_view_mode'];
} else {
    $select_view_mode = 'Таблица с rowspan и 1 колонкой требований';
}

if (array_key_exists('select_instruction_view', $postVars)) {
    $select_instruction_view = $postVars['select_instruction_view'];
} else {
    $select_instruction_view = 'Инструкция без параграфов без списков';
}

if (array_key_exists('select_instruction_mode', $postVars)) {
    $select_instruction_mode = $postVars['select_instruction_mode'];
} else {
    $select_instruction_mode = 'Требования цифрами';
}

if (array_key_exists('select_view_mode_app', $postVars)) {
    $select_view_mode_app = $postVars['select_view_mode_app'];
} else {
    $select_view_mode_app = 'Нет';
}

if (array_key_exists('select_view_mode_req_numbers', $postVars)) {
    $select_view_mode_req_numbers = $postVars['select_view_mode_req_numbers'];
} else {
    $select_view_mode_req_numbers = 'Текст';
}

if (array_key_exists('select_view_mode_app_numbers', $postVars)) {
    $select_view_mode_app_numbers = $postVars['select_view_mode_app_numbers'];
} else {
    $select_view_mode_app_numbers = 'Текст';
}

if (array_key_exists('select_project', $postVars)) {
    $select_project = $postVars['select_project'];
    $posSpace = strpos($postVars['select_project'], " ");

    $posSpace == 0 ?
        $select_project_id = (int)($select_project) :
        $select_project_id = (int)(substr($postVars['select_project'], 0, $posSpace));

} else {
    $select_project = '';
    $select_project_id = '';
}

if ($_GET['checkpoint_project'] != '' ) {
    $select_project = $_GET['checkpoint_project'];
    $select_project_id = (int)($_GET['checkpoint_project']);
}

if (array_key_exists('select_checkpoint', $postVars)) {
    $select_checkpoint = (int)($postVars['select_checkpoint']);
    $posSpace = strpos($postVars['select_checkpoint'], " ");

    $posSpace == 0 ?
        $select_checkpoint_num = (int)($select_project) :
        $select_checkpoint_num = (int)(substr($postVars['select_checkpoint'], 0, $posSpace));

    if ((int)(getCheckpointIdByNum($mysqli, $select_project_id, $select_checkpoint_num)) == -1) {
        $select_checkpoint = '';
    }
} else {
    $select_checkpoint = '';
}

if ($_GET['create_checkpoint'] != '' ) {
    $created_checkpoint_id = (int)($_GET['create_checkpoint']);
    $after_create_checkpoint = true;
}


$form_action = '';
if (array_key_exists('form_action', $postVars)) {
    $form_action = (int)($postVars['form_action']);
}
?>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<script type="text/javascript" src="scripts/script.js?<?= (int)(microtime(1)) ?>"></script>
<?= $message; ?>

<div class='div_table'>
    <hr>
    <div class="div_group div_top_valign">
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Введите номер проекта:</div>
                <div>
                    <input type="text" name="project"/>
                </div>
            </div>
            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Загрузка проекта:</div>
                <div>
                    <input type="file" name="file"/>
                    <button>Загрузить</button>
                    <?= $messageLoadProject; ?>
                </div>
            </div>
        </form>
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Загрузка каталога:</div>
                <div>
                    <input type="file" name="catalog"/>
                    <button>Загрузить</button>
                    <?= $messageLoadCatalog; ?>
                </div>
            </div>
        </form>

        <form action="index.php" method="POST" id="mainform">
        <hr>
            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Проект:</div>
                <div>
                    <select id='select_project' name='select_project' onchange='FillCheckpoints();'>
                        <option></option>
                        <?php
                        foreach ($projects as $nextProject) {
                            $projectId = $nextProject['ProjectId'];
                            $projectTitle = $nextProject['ProjectTitle'];

                            $select_project == trim($projectId . " " . $projectTitle) ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$projectId} {$projectTitle}</option>\r\n";
                        }
                        ?>
                    </select>
                    Версия: <select id='select_project' name='select_project_version' onchange='FillCheckpoints();'>
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                    <div class='div_col div_col_sm2 '>Контрольная точка:</div>
                    <div>
                        <select id='select_checkpoint' name='select_checkpoint'>
                            <option></option>
                            <?php
                            if ($select_project_id <> "" && array_key_exists($select_project_id, $checkpoints)) {
                                $i = 1;
                                $countCheckpoints = count($checkpoints[$select_project_id]);

                                foreach ($checkpoints[$select_project_id] as $checkpoint) {
                                    $checkpointId = $checkpoint['id'];
                                    $checkpointNum = $checkpoint['num'];
                                    $checkpointDateTime = $checkpoint['datetime'];

                                    if ($select_checkpoint == '' && $countCheckpoints == $i) {
                                        $selected = ' selected';
                                    } else {
                                        $select_checkpoint == trim($checkpointNum . " " . $checkpointDateTime) ? $selected = ' selected' : $selected = '';
                                    }

                                    if ($after_create_checkpoint && $checkpointId===$created_checkpoint_id ){
                                        $selected = ' selected';
                                    }

                                    echo "<option$selected>{$checkpointNum} {$checkpointDateTime}</option>\r\n";
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                        <button onclick='CheckProjects("<?php echo $baseUrl; ?>", 1, 1);return false;'>Слч-мат,Слч-треб</button>
                        <button onclick='CheckProjects("<?php echo $baseUrl; ?>", 0, 1);return false;'>Исх-мат,Слч-треб</button>
                        <button onclick='CheckProjects("<?php echo $baseUrl; ?>", 1, 0);return false;'>Слч-мат,Исх-треб</button>
                        <button onclick='CheckProjects("<?php echo $baseUrl; ?>", 0, 0);return false;'>Исх-мат,Исх-треб</button>
                        <?= $messageCreateCheckpoint; ?>
                    </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Данные заявки:</div>
                <div>
                    <select name='select_view_mode_app'>
                        <?php
                        foreach ($viewModesApp as $viewModeApp) {
                            $select_view_mode_app == $viewModeApp ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$viewModeApp}</option>\r\n";
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Вывод цифровых значений ТЗ:</div>
                <div>
                    <select name='select_view_mode_req_numbers'>
                        <?php
                        foreach ($viewModesNumbers as $viewModeNumbers) {
                            $select_view_mode_req_numbers == $viewModeNumbers ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$viewModeNumbers}</option>\r\n";
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Вывод цифровых значений заявки:</div>
                <div>
                    <select name='select_view_mode_app_numbers'>
                        <?php
                        foreach ($viewModesNumbers as $viewModeNumbers) {
                            $select_view_mode_app_numbers == $viewModeNumbers ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$viewModeNumbers}</option>\r\n";
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Режим отображения:</div>
                <div>
                    <select name='select_view_mode'>
                        <?php
                        foreach ($viewModes as $viewMode) {
                            $select_view_mode == $viewMode ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$viewMode}</option>\r\n";
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Вид инструкции:</div>
                <div>
                    <select name='select_instruction_view'>
                        <?php
                        foreach ($instructionViews as $instructionView) {
                            $select_instruction_view == $instructionView ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$instructionView}</option>\r\n";
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>Режим инструкции:</div>
                <div>
                    <select name='select_instruction_mode'>
                        <?php
                        foreach ($instructionModes as $instructionMode) {
                            $select_instruction_mode == $instructionMode ? $selected = ' selected' : $selected = '';
                            echo "<option$selected>{$instructionMode}</option>\r\n";
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class='div_row div_top_valign'>
                <div class='div_col div_col_sm2 '>
                    <input class='button' type='submit' name='generate_button' value='Сгенерировать'>
                </div>
            </div>
            <input type="hidden" name='form_action' value=''>
        </form>
        <hr>
    </div>

</div>

<div class='div_console'>

    <?php
    if (!$after_create_checkpoint && $select_project_id <> "" && $select_view_mode <> "" && $select_checkpoint <> "") {
        $modeResult = array_search($select_view_mode, $viewModes);
        $projectId = $select_project_id;
        $checkpointNum = $select_checkpoint_num;

        require 'generate.php';

        $select_view_mode_app == "Да" ? $fileSuffix = "_app" : $fileSuffix = "_req";

        $curTime = date('Ymd_His');
        $filename = $projectId . "_" . $checkpointNum . $fileSuffix . "_" . $curTime;

        $file = fopen($baseFolder . $filename . ".txt", "w+");
        if ($file) {
            fwrite($file, $strResult);
            fclose($file);

            echo "<a href='$baseUrl/reports/$filename.txt' target='_blank'>$filename</a><br><br>";
        }

        echo str_replace("\n", "<br>", str_replace(" ", "&nbsp;", $strResult));

    }
    ?>
</div>

<script type="text/javascript">
    <?php
    $strCheckpointsArray = "var checkpoints = [";

    $projects = implode(",", $projects);
    $strProjectsArray = "var projects = [$projects];\r\n";
    echo $strProjectsArray;

    foreach ($checkpoints as $projectId => $checkpointByProject) {
        $strCheckpointsArray .= "$projectId, ";
    }
    $strCheckpointsArray .= "]";
    //echo $strCheckpointsArray;
    ?>
</script>

</body>
</html>


<?php
if (is_object($mysqli) && $mysqli->ping()) $mysqli->close();
?>

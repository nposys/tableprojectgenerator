<?php

$operators = [];

// нижние границы
$operators['min_value'][0][] = 'более чем';
$operators['min_value'][0][] = 'больше чем';
$operators['min_value'][0][] = 'выше чем';
$operators['min_value'][0][] = 'должно быть больше чем';
$operators['min_value'][0][] = 'должно быть более чем';
$operators['min_value'][0][] = 'должно быть выше чем';
$operators['min_value'][0][] = 'должно превышать';

$operators['min_value'][1][] = 'не менее чем';
$operators['min_value'][1][] = 'не меньше чем';
$operators['min_value'][1][] = 'не ниже чем';
$operators['min_value'][1][] = 'должно быть не меньше чем';
$operators['min_value'][1][] = 'должно быть не менее чем';
$operators['min_value'][1][] = 'должно быть не ниже чем';
$operators['min_value'][1][] = 'от';

// верхние границы
$operators['max_value'][0][] = 'менее чем';
$operators['max_value'][0][] = 'меньше чем';
$operators['max_value'][0][] = 'ниже чем';
$operators['max_value'][0][] = 'должно быть меньше чем';
$operators['max_value'][0][] = 'должно быть менее чем';
$operators['max_value'][0][] = 'должно быть ниже чем';

$operators['max_value'][1][] = 'не более чем';
$operators['max_value'][1][] = 'не больше чем';
$operators['max_value'][1][] = 'не выше чем';
$operators['max_value'][1][] = 'должно быть не больше чем';
$operators['max_value'][1][] = 'должно быть не более чем';
$operators['max_value'][1][] = 'должно быть не выше чем';
$operators['max_value'][1][] = 'не должно превышать';
$operators['max_value'][1][] = 'до';

function getMinValue($row, $bGetRandomText = false, $bConvertNumToWord = true)
{
    global $operators;

    if ($row['MinRange'] <> '') {
        $strValue = $row['MinRange'];

        if (is_numeric(str_replace(",", ".", $strValue))) {
            $bConvertNumToWord ? $strValue = num2str_fraction(str_replace(",", ".", $strValue), "", "none", 0) : '';

            $bGetRandomText ?
                $strOperator = getRandomValue($operators['min_value'][$row['isMinInclude']]) :
                $strOperator = $operators['min_value'][$row['isMinInclude']][0];

            if ($row['isMinInclude'] == 1) {
                if ($row['isRange'] == 1) {
                    return 'от не выше чем ' . $strValue;
                } else {
                    return $strOperator . " " . $strValue;
                } // 'не менее чем '
            } else {
                if ($row['isRange'] == 1) {
                    return 'от ниже чем ' . $strValue; // 'более чем '
                } else {
                    return $strOperator . " " . $strValue; // 'более чем '
                }
            }
        } else {
            if ($row['isMinInclude'] == 1){
                return 'не ниже чем ' . $strValue;
            } else {
                return 'выше чем ' . $strValue;
            }
        }
    }

    return '';
}

function getMaxValue($row, $bGetRandomText = false, $bConvertNumToWord = true)
{
    global $operators;

    if ($row['MaxRange'] <> '') {
        $strValue = $row['MaxRange'];
        if (is_numeric(str_replace(",", ".", $strValue))) {
            $bConvertNumToWord ? $strValue = num2str_fraction(str_replace(",", ".", $strValue), "", "none", 0) : '';

            $bGetRandomText ?
                $strOperator = getRandomValue($operators['max_value'][$row['isMaxInclude']]) :
                $strOperator = $operators['max_value'][$row['isMaxInclude']][0];

            if ($row['isMaxInclude'] == 1) {
                if ($row['isRange'] == 1) {
                    return 'до не ниже чем ' . $strValue;
                } else {
                    return $strOperator . " " . $strValue;
                }
            } else {
                if ($row['isRange'] == 1) {
                    return 'до выше чем ' . $strValue;
                } else {
                    return $strOperator . " " . $strValue;
                }
            }
        } else {
            if ($row['isMaxInclude'] == 1) {
                return 'не выше чем ' . $strValue;
            } else {
                return 'ниже чем ' . $strValue;
            }
        }
    }
    return '';
}

function getRangeValue($row, $bGetRandomText = false, $bConvertNumToWord = true)
{
    global $operators;

    if ($row['MinRange'] <> '' && $row['MaxRange'] <> '') {
        $strBottom = getMinValue($row, $bGetRandomText, $bConvertNumToWord);
        $strTop = getMaxValue($row, $bGetRandomText, $bConvertNumToWord);

        if ($row['isRange'] == 1) {
            $strResult = trim($strBottom . " " . $strTop);
        } else {
            if ($bGetRandomText && mt_rand(0, 1) == 0) {
                $strResult = trim($strTop . " и " . $strBottom);
            } else {
                $strResult = trim($strBottom . " и " . $strTop);
            }
        }

        return $strResult;
    }

    return '';
}

function getFixedValue($row, $bGetRandomText = false, $bConvertNumToWord = true)
{
    global $operators;

    if ($row['FixedValue'] <> '') {
        $strValue = $row['FixedValue'];
        is_numeric(str_replace(",", ".", $strValue))
            ? $strValue = num2str_fraction(str_replace(",", ".", $strValue), "", "none", 0)
            : '';

        if ($row['NegativeAssert'] == 1) {
            return 'не должно быть ' . $strValue;
        } else {
            return 'должно быть ' . $strValue;
        }
    }

    return '';
}

function getValue($row, $bGetRandomText = false, $bConvertNumToWord = true)
{
    if ($row['FixedValue'] <> '') {
        return getFixedValue($row, $bGetRandomText, $bConvertNumToWord);
    } elseif ($row['MinRange'] <> '' && $row['MaxRange'] == '') {
        return getMinValue($row, $bGetRandomText, $bConvertNumToWord);
    } elseif ($row['MinRange'] == '' && $row['MaxRange'] <> '') {
        return getMaxValue($row, $bGetRandomText, $bConvertNumToWord);
    } elseif ($row['MinRange'] <> '' && $row['MaxRange'] <> '') {
        return getRangeValue($row, $bGetRandomText, $bConvertNumToWord);
    }
}

function getAppValue($row, $bConvertNumToWord = false)
{
    $strResult = "";

    if ($bConvertNumToWord) {
        $strValue = $row['OrderValue'];
        is_numeric(str_replace(",", ".", $strValue))
            ? $strValue = num2str_fraction(str_replace(",", ".", $strValue), "", "none", 0)
            : '';

        $strResult = $strValue;
    } else {
        $strResult = $row['OrderValue'];
    }

    if (trim($row['Additional'])) {
        $strResult .= " " . trim($row['Additional']); // " (интервальное значение, указано в соответствии с данными производителя товара)";
    }

    return $strResult;
}

function getAppTradeMark($row)
{
    return ($row['TradeMark']);
}

function getAppCountry($row)
{
    return ($row['Country']);
}

function getRandomValue($valuesArray)
{
    if (is_array($valuesArray)) {
        $numValues = count($valuesArray);
        if ($numValues > 0) {
            $i = mt_rand(0, $numValues - 1);
            return $valuesArray[$i];
        }
    }

    return "";
}

function prepareTextForString ($text, $bGenerateLists = false, $bAddQuotes = true, $bIsLast = false)
{
    $bAddQuotes ? $result = "'".$text."'" : $result = $text;
    $bIsLast ? $result = $result : $result = $result.", ";
    $bGenerateLists ? $result = "- ".$result."\r\n" : $result = $result;

    return $result;
}

function generateInstruction($instruction, $bGenerateParagraphs = false, $bGenerateLists = false, $bAddQuotes = true)
{
    $strInstruction = "";

    if (is_array($instruction)) {
        foreach ($instruction as $materialTitle => $params) {
            if (array_key_exists(2, $params)) {
                $strParams = "";
                $num_params = count($params[2]);
                for ($i = 0; $i < $num_params; $i++) {
                    $paramTitle = $params[2][$i];
                    $strParams .= prepareTextForString ($paramTitle, $bGenerateLists, $bAddQuotes, $i == ($num_params - 1));
                }

                if ($strParams <> "") {
                    $bGenerateLists ?
                        $strInstruction .= "При указании значений, соответствующих требованиям технического задания по показателю(ям):\r\n$strParams\r\nтовара/группы требований '$materialTitle' в заявке на участие требуется указание участником закупки интервального значения показателя. " :
                        $strInstruction .= "При указании значений, соответствующих требованиям технического задания по показателю(ям) $strParams товара/группы требований '$materialTitle' в заявке на участие требуется указание участником закупки интервального значения показателя. ";
                    $bGenerateParagraphs ? $strInstruction = $strInstruction . "\r\n\r\n" : '';
                }
            }

            if (array_key_exists(1, $params)) {
                $strParams = "";
                $num_params = count($params[1]);
                for ($i = 0; $i < $num_params; $i++) {
                    $paramTitle = $params[1][$i];
                    $strParams .= prepareTextForString ($paramTitle, $bGenerateLists, $bAddQuotes, $i == ($num_params - 1));
                }

                if ($strParams <> "") {
                    $bGenerateLists ?
                        $strInstruction .= "При указании значений, соответствующих требованиям технического задания по показателю(ям):\r\n$strParams\r\nтовара/группы требований '$materialTitle' в заявке на участие допускается указание участником закупки интервального значения показателя, в случае если такой показатель установлен в ГОСТ в виде интервала значений. " :
                        $strInstruction .= "При указании значений, соответствующих требованиям технического задания по показателю(ям) $strParams товара/группы требований '$materialTitle' в заявке на участие допускается указание участником закупки интервального значения показателя, в случае если такой показатель установлен в ГОСТ и/или производителем товара в виде интервала значений. ";
                    $bGenerateParagraphs ? $strInstruction = $strInstruction . "\r\n\r\n" : '';
                }
            }

            // -----------------------------------------------
            if (array_key_exists(0, $params)) {
                $strParams = "";
                $num_params = count($params[0]);
                for ($i = 0; $i < $num_params; $i++) {
                    $paramTitle = $params[0][$i];
                    $strParams .= prepareTextForString ($paramTitle, $bGenerateLists, $bAddQuotes, $i == ($num_params - 1));
                }

                if ($strParams <> "") {
                    $bGenerateLists ?
                        $strInstruction .= "При указании значений, соответствующих требованиям технического задания по показателю(ям):\r\n$strParams\r\nтовара/группы требований '$materialTitle' в заявке на участие требуется указание участником закупки фиксированного значения (в виде одного значения или перечисления значений, если такое перечисление допускается требованиями настоящего технического задания) такого(таких) показателя(показателей). " :
                        $strInstruction .= "При указании значений, соответствующих требованиям технического задания по показателю(ям) $strParams товара/группы требований '$materialTitle' в заявке на участие требуется указание участником закупки фиксированного значения (в виде одного значения или перечисления значений, если такое перечисление допускается требованиями настоящего технического задания) такого(таких) показателя(показателей). ";
                    $bGenerateParagraphs ? $strInstruction = $strInstruction . "\r\n\r\n" : '';
                }
            }
        }
    } // end if

    return trim($strInstruction);
}

function cleanString($string)
{
    return trim(
            preg_replace('/\s{2,}/', ' ',
                str_replace("\n", " ",
                    str_replace("\r", " ", $string))));
}

function getMaxCheckpointNum($mysqli, $projectId)
{
    if ($mysqli->ping() && $projectId <> "" && is_int($projectId)) {
        $queryCheckpoints = "SELECT MAX(NumCheckpoint) as NumCheckpoint FROM project.Checkpoint WHERE ProjectId = $projectId";
        $queryCheckpointResult = $mysqli->query($queryCheckpoints);
        $checkpointId = 0;
        if ($rowCheckpoint = $queryCheckpointResult->fetch_assoc()) {
            $checkpointId = (int)($rowCheckpoint['NumCheckpoint']);
        }
        return $checkpointId;
    } else {
        return -1;
    }
}

function getCheckpointIdByNum($mysqli, $projectId, $checkpointNum = 0)
{
    if ($mysqli->ping() && $projectId <> "" && is_int($projectId)) {
        if ($checkpointNum == 0) {
            $checkpointNum = getMaxCheckpointNum($mysqli, $projectId);
        }

        if ($checkpointNum > 0) {
            $queryCheckpoints = "SELECT CheckpointId FROM project.Checkpoint WHERE ProjectId = $projectId and NumCheckpoint=$checkpointNum";
            $queryCheckpointResult = $mysqli->query($queryCheckpoints);
            if ($rowCheckpoint = $queryCheckpointResult->fetch_assoc()) {
                return (int)($rowCheckpoint['CheckpointId']);
            }
        }
    }

    return -1;
}

function insertCheckpoint($mysqli, $projectId, $checkpointNum)
{
    if ($mysqli->ping() && is_int($projectId) && is_int($checkpointNum)) {
        $datetime = date("Y-m-d H:i:s");

        $queryInsertCheckpoint =
            "INSERT INTO
                project.Checkpoint
             (DateTime, ProjectId, NumCheckpoint)
             VALUES
             ('$datetime', $projectId, $checkpointNum )
            ";

            $mysqli->real_query($queryInsertCheckpoint);
    }
}

function getRealPOST()
{
    $pairs = explode("&", file_get_contents("php://input"));
    $vars = array();
    foreach ($pairs as $pair) {
        $nv = explode("=", $pair);
        if (array_key_exists(0, $nv) && array_key_exists(1, $nv)) {
            $name = urldecode($nv[0]);
            $value = urldecode($nv[1]);
            $vars[$name] = $value;
        }
    }
    return $vars;
}

function returnStringForDownload($str, $filename)
{
    if ($filename <> "" && $str <> "") {
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Type: text/plain'); # Don't use application/force-download - it's not a real MIME type, and the Content-Disposition header is sufficient
        header('Content-Length: ' . strlen($str));
        header('Connection: close');

        echo $str;
    }
}

function loadFile($db, $file, $project, $ver)
{
    /** @var Mysqli $db */
    if (!$db->query("DELETE FROM `_RawData`")) return false;
    $load = $db->query("LOAD DATA INFILE '{$file}' INTO TABLE `_RawData` CHARACTER SET 'cp1251'
				FIELDS TERMINATED BY ';' 
				ENCLOSED BY '\"' 
				LINES TERMINATED BY '\r\n'
				IGNORE 1 LINES
				(MaterialTitle, ParameterTitle, MinInclude, Min, MaxInclude, 
				Max, isRange, FixedValue, isNo, Unit, ShortUnit, Comment, 
				AppValue, Additional, OrderRange, GroupTitle)");
    if ($load && normalizeDb($db, $project, $ver)) return true;
    return false;
}

function loadCatalog($db, $file, $project, $ver)
{
    /** @var Mysqli $db */
    if (!$db->query("DELETE FROM `_RawCatalog`")) return false;

    $load = $db->query("LOAD DATA INFILE '{$file}' INTO TABLE `_RawCatalog` CHARACTER SET 'cp1251'
				FIELDS TERMINATED BY ';' 
				ENCLOSED BY '\"' 
				LINES TERMINATED BY '\r\n'
				IGNORE 1 LINES
				(MaterialTitle, TradeMark, Country)");
    if ($load) return true;
    return false;
}

function checkProject($db, $project, $ver)
{
    if (!$project || !$ver) return false;
    /** @var Mysqli $db */
    $row = $db->query("SELECT Id FROM Item WHERE ProjectId = '{$project}' AND Version = '{$ver}'");

    return $row->num_rows > 0 ? false : true;
}

function normalizeDb($db, $project, $ver)
{
    /** @var Mysqli $db */
    $db->query("DELETE FROM `_RawData` WHERE MaterialTitle = '' OR ParameterTitle = '';");
    $res = $db->query("CALL normalize_v2({$project}, {$ver});");
    return $res;
}

function getMaxVersion($db, $project)
{
    if (!$project) return false;
    /** @var Mysqli $db */
    $row = $db->query("SELECT max(Version) as Version FROM Item WHERE ProjectId = '{$project}'");
    $obj = $row->fetch_object();
    return ++$obj->Version;
}
